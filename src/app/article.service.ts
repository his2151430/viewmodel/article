import { Article } from "./article";

export interface ArticleService {
  
  getArticles(): Promise<Article[]>;

  removeArticle(id: number): any;

  modifyArticle(article: Article):any;

  addArticle(article:Article):Promise<Article[]>;
}
