export class Article {
  id: number;
  href: string;
  title: string;
  date: string;
  author: string;
  category: string;
  categoryLink: string;
  summary: string;

  constructor(
    href: string,
    title: string,
    date: string,
    author: string,
    category: string,
    categoryLink: string,
    summary: string
  ) {
    this.id = 0;
    this.title = title || "";
    this.href = href || "";
    this.date = date || "";
    this.author = author || "";
    this.category = category || "";
    this.categoryLink = categoryLink || "";
    this.summary = summary || "";
  }
}
